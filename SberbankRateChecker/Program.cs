﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading;
using RestSharp;

namespace SberbankRateChecker
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                double percentDifference = 100.00;
                var currencies = ParseCurrecies();
                var usd = currencies.FirstOrDefault(c => c[0].Equals("USD", StringComparison.OrdinalIgnoreCase));
                if (usd != null)
                {
                    var bankBuy = float.Parse(usd[2]);
                    var bankSell = float.Parse(usd[3]);
                    percentDifference = Math.Round((1 - bankBuy / bankSell) * 100);
                }
                if (percentDifference < 50)
                {
                    EmailNotifier(percentDifference);
                }

                Thread.Sleep(1000 * 5 * 1); // run every 30 minutes
            }
        }

        private static List<string[]> ParseCurrecies()
        {
            string date = String.Format("{0:dd.MM.yyyy}", DateTime.Now);
            const string sberbankRateUrl = "http://www.sberbank.ru/common/js/quote_table.php";
            var client = new RestClient(sberbankRateUrl);
            var request = new RestRequest(Method.POST);
            request.AddParameter("cbrf", "1");
            request.AddParameter("inf_block", "163");
            request.AddParameter("quotes_for", "");
            request.AddParameter("version", "0");
            request.AddParameter("site", "1");
            request.AddParameter("date", date);
            request.AddParameter("payment", "cash");
            request.AddParameter("person", "natural");

            // easily add HTTP Headers
            request.AddHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36");

            IRestResponse response = client.Execute(request);
            var currencies = new List<string[]>();
            var pattern = @"<tr>.*?</tr>";
            Regex regex = new Regex(pattern);
            foreach (Match m in regex.Matches(response.Content))
            {
                var currencyData = m.Value;
                var innerPattern = @"<td .*?>(\w{3}|\d{1,5}.?\d{0,5})(<|\s)";
                Regex innerRegex = new Regex(innerPattern);
                var currency = new List<string>();
                foreach (Match row in innerRegex.Matches(currencyData))
                {
                    currency.Add(row.Groups[1].Value);
                }
                currencies.Add(currency.ToArray());
            }

            return currencies;
        }

        private static void EmailNotifier(double percentDifference)
        {
            var fromAddress = new MailAddress("korvinio@gmail.com", "Sberbank checker");
            var toAddress = new MailAddress("kambersky@gmail.com", "Sergei");
//            var toAddress = new MailAddress("8941150@mail.ru", "Sergei");
            const string fromPassword = "dima!23pas";
            const string subject = "Best Sberbank Exchange Rate";
            string body = string.Format("Best exchange difference: {0}%", percentDifference);

            var smtp = new SmtpClient
            {
                Host = "smtp.googlemail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                smtp.Send(message);
            } 
        }
    }
}
